//
// Created by "Beat Wolf" on 08.02.2022.
//
#include <fstream>
#include <iostream>
#include <sstream>
#include "src/Point.h"
#include "src/ClosestPair.h"
class StarbucksReducer{
    /*
     * The purpose of this class is for better code readability
     * by separating the code that would go in the main function into
     * different functions.
     * */
public:
    StarbucksReducer()= default;
    bool getData(){
        /*
         * Cette méthode sert à lire le contenu d'un fichier .tsv et de placer son contenu dans le vecteur data_*/
        std::ifstream inputFile(inputDataPath_, std::ios::in);
        if (inputFile){
            std::string line;
            size_t position_delimiter;
            std::string delimiter="\t";
            while(std::getline(inputFile, line)){
                std::vector<std::string> data_section= std::vector<std::string>();
                while ((position_delimiter=line.find(delimiter))!=std::string::npos){
                    data_section.push_back(line.substr(0,position_delimiter-1));
                    line.erase(0,position_delimiter+delimiter.length());
                }
                data_section.push_back(line);
                data_.push_back(data_section);
            }
            inputFile.close();
            return true;
        }else{
            std::cout<<"No file found.";
            return false;
        }
    }

    bool postData(){
        /*
         * Cette méthode sert à écrire le contenu de l'ensemble "locations" dans un fichier d'output.
         * */
        std::ofstream outputFile(outputDataPath,std::ios::out);
        if (outputFile){
            std::vector<Point *> locations= reduceStarbucks_();
            outputFile<<"Store Name(s)\tLongitude\tLatitude\n";
            for(Point* location:locations){
                std::string locationData=location->getName()+"\t"+std::to_string(location->getX())
                                                                  +"\t"+std::to_string(location->getY())+"\n";
                outputFile<<locationData;
            }
            outputFile.close();
            return true;
        } else{
            std::cout<<"Couldn't open or write file.";
            return false;
        }
    }
private:

    std::vector<Point*> getLocations_(){
        int data_control=0;
        std::vector<Point *> locations;

        for(std::vector<std::string> section: data_){
            if (data_control==0){//ce check est fait pour éviter d'avoir le titre des données
                // qui s'ajouterait aux données
                data_control++;
                continue;
            }
            //créer un point avec les données de longitude, latitude et le nom et l'ajouter au vector locations
            std::string name=section[name_index_];
            std::string longitudeTxt=section[longitude_index_];
            std::string latitudeTxt=section[latitude_index_];
            double longitude= std::stod(longitudeTxt);
            double latitude= std::stod(latitudeTxt);
            locations.push_back(new Point(longitude,latitude,name));
        }
        return locations;
    }

    std::vector<Point*> reduceStarbucks_(){
        ClosestPair closestPair;
        std::vector<Point*> locations= getLocations_();
        double numberOfLocationsAfterReduction=locations.size()*9/10;
        int size=locations.size();
        while (size>numberOfLocationsAfterReduction){//tant que la taille des données est plus grande que 90%
            std::pair<Point *, Point *> pair=closestPair.closestPair(locations);//trouver la paire la plus proche
            //calculer et créer le nouveau point entre deux puis l'ajouter
            double newPointLongitude=(pair.first->getX()+pair.second->getX())/2;
            double newPointLatitude=(pair.first->getY()+pair.second->getY())/2;
            std::string newPointName= pair.first->getName()+pair.second->getName();
            Point combinedLocations= Point(newPointLongitude,newPointLatitude,newPointName);
            locations.push_back(&combinedLocations);
            //trouver où sont les points de la paire et les enlever
            auto pointLocator=std::find(locations.begin(),locations.end(),pair.first);
            locations.erase(pointLocator);
            pointLocator=std::find(locations.begin(),locations.end(),pair.second);
            locations.erase(pointLocator);
            size=locations.size();
        }
        //retourner le nouvel ensemble de lieux réduit de 10%
        return locations;
    }
    std::vector<std::vector<std::string>> data_;
    std::string const inputDataPath_="../data/us_starbucks.tsv";
    std::string const outputDataPath="../data/reduced_us_starbucks.tsv";
    int const name_index_=2;
    int const longitude_index_=11;
    int const latitude_index_=12;
};

int main() {
    StarbucksReducer starbucksReducer;
    if (starbucksReducer.getData()){
        if (starbucksReducer.postData()){
            return EXIT_SUCCESS;
        }
    }
    return EXIT_FAILURE;
}