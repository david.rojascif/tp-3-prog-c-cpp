//
// Created by "Beat Wolf" on 08.02.2022.
//

#include "ClosestPair.h"

ClosestPair::ClosestPair(){
    const auto comp = []( const Point * a, const Point * b){
        if(a->getY() == b->getY()){
            return a->getX() < b->getX();
        }
        return a->getY() < b->getY();
    };

    candidates = std::set<Point *, std::function<bool(const Point *, const Point *)>>(comp);
}

std::pair<Point *, Point *> ClosestPair::closestPair(std::vector<Point *> &searchPoints){
    //Init
    points = searchPoints;
    candidates.clear();
    leftMostCandidate = 0;

    //Init solution
    solution = std::make_pair(points[0], points[1]);
    smallestDistance = solution.first->distance(*solution.second);

    for (int i=0; i<(int)(points.size());i++){
        Point *p=points[i];
        handleEvent(p);
    }


    return solution;
}

void ClosestPair::handleEvent(Point *p){
    shrinkCandidates(p);
    //lowerSearch et upperSearch sont mis à jour afin de représenter les limites de la zone de détection
    // pour les candidats
    lowerSearch.setX(p->getX()-smallestDistance);
    lowerSearch.setY(p->getY()-smallestDistance);//la coordonée Y est calculée de telle façon que le point p
    // se trouve au milieu verticalement de la zone
    upperSearch.setX(p->getX());
    upperSearch.setY(p->getY()+smallestDistance);
    //En vérifiant un point à la fois, si le point entre dans la zone délimitée par lowerSearch et upperSearch
    //alors il fait partie des candidats et il faut exclure le point p de la liste car
    //la paire doit être composée de deux points différents.
    for (int i=0;i<(int)points.size();i++){
        Point* leftP=points[i];
        if ((lowerSearch.getY()<=leftP->getY()&&upperSearch.getY()>=leftP->getY())&&
        (lowerSearch.getX()<=leftP->getX()&&upperSearch.getX()>=leftP->getX())){
            if (leftP!=p) candidates.insert(leftP);
        }
    }
    //en prenant les candidats un à la fois, leur distance vers le point p est calculée et si elle est plus petite
    // que la valeur minimale actuelle alors la distance minimale et la solution sont mis à jour.
    for(Point *candidate:candidates){
        if (candidate==p)continue;
        double distCheck=candidate->distance(*p);
        if (smallestDistance>distCheck){
            smallestDistance=distCheck;
            solution = std::make_pair( candidate, p);
        }
    }
}

void ClosestPair::shrinkCandidates(const Point *p){
    while(p->getX() - points[leftMostCandidate]->getX()> smallestDistance){
        candidates.erase(points[leftMostCandidate]);

        leftMostCandidate++;
    }
}