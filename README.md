# cpp-closestneighbours

Resultats des tests:

a)  "ClosestPair: Test random"

    Using random points,
    nbOfPoints:500,
    nbOfDataSets:5,
    nbOfCalls:1
    NaivePairs run in 22 ms
    FastPairs run in 10 ms
>Impressions:
> >Comme cela était espéré, la version de l'algorithme utilisé lors du run "FastPairs" est plus rapide que 
> la version naïve.

b) "ClosestPair: Test many small instances"

    Using random points,
    nbOfPoints:10,
    nbOfDataSets:500,
    nbOfCalls:1
    NaivePairs run in 0 ms
    FastPairs run in 2 ms
>Impressions:
> >Étonnament, la version naïve est présentée comme étant très rapide comparée à l'autre mais
> en inspectant le debuggeur, l'arrondissage des valeurs des temps est la cause de ce résultat.
> Il n'en est moins qu'en même que la version naïve est plus rapide avec peu de points mais vraisemblablement de peu.

c) "ClosestPair: Test vertically aligned"

    Using worst case points,
    nbOfPoints:500,
    nbOfDataSets:5,
    nbOfCalls:1
    NaivePairs run in 12 ms
    FastPairs run in 38 ms
>Impressions:
> >Lorsque les points sont alignés verticalement(étant le pire scenario), la version 
> "FastPairs" de l'algorithme tarde beaucoup plus que la version naïve car beaucoup moins 
> de points sont retirés de la liste des candidats vu que les points alignés verticalement sont plus proches
> horizontalement les uns des autres.

d) "ClosestPair: Benchmark"

    section random:
    Using random points,
    nbOfPoints:200,
    nbOfDataSets:5,
    nbOfCalls:10
    NaivePairs run in 45 ms
    FastPairs run in 36 ms

    Using random points,
    nbOfPoints:400,
    nbOfDataSets:5,
    nbOfCalls:10
    NaivePairs run in 198 ms
    FastPairs run in 183 ms

    Using random points,
    nbOfPoints:800,
    nbOfDataSets:5,
    nbOfCalls:10
    NaivePairs run in 632 ms
    FastPairs run in 467 ms


    section worst-case:
    Using worst case points, 
    nbOfPoints:200,
    nbOfDataSets:5,
    nbOfCalls:10
    NaivePairs run in 44 ms
    FastPairs run in 58 ms
    
    Using worst case points,
    nbOfPoints:400,
    nbOfDataSets:5,
    nbOfCalls:10
    NaivePairs run in 158 ms
    FastPairs run in 203 ms
    
    Using worst case points,
    nbOfPoints:800,
    nbOfDataSets:5,
    nbOfCalls:10
    NaivePairs run in 698 ms
    FastPairs run in 782 ms

>Impressions:
> >Le résultat de cette série de tests confirme qu'il y a une perte de performance croissante si la version
> naïve n'est pas utilisée dans le pire des cas mais autrement, l'autre version de l'algorithme est plus rapide.